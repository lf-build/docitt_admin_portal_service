FROM openresty/openresty:1.17.8.2-5-bionic

RUN /usr/local/openresty/luajit/bin/luarocks install lua-cjson
RUN /usr/local/openresty/luajit/bin/luarocks install lua-resty-string
RUN /usr/local/openresty/luajit/bin/luarocks install lua-resty-mlcache

ADD lua /etc/nginx/lua
ADD nginx.conf /usr/local/openresty/nginx/conf/nginx.conf
ADD error.html /src/dist/error.html
ADD favicon.ico /src/dist/favicon.ico

EXPOSE 5000

ENTRYPOINT \
    sed -i 's~CONFIG_HOST~'${CONFIGURATION_HOST}'~g' /usr/local/openresty/nginx/conf/nginx.conf  && \
    sed -i 's~SECURITY_HOST~'${SECURITY_HOST}'~g' /usr/local/openresty/nginx/conf/nginx.conf  && \
    sed -i 's~ASSETMANAGER_HOST~'${ASSETMANAGER_HOST}'~g' /usr/local/openresty/nginx/conf/nginx.conf  && \
    sed -i 's~THEME_HOST~'${THEME_HOST}'~g' /usr/local/openresty/nginx/conf/nginx.conf  && \
    sed -i 's~TEMPLATEMANAGER_HOST~'${TEMPLATEMANAGER_HOST}'~g' /usr/local/openresty/nginx/conf/nginx.conf  && \
    sed -i 's~QUESTIONNAIRE_HOST~'${QUESTIONNAIRE_HOST}'~g' /usr/local/openresty/nginx/conf/nginx.conf  && \
    sed -i 's~USERPROFILE_HOST~'${USERPROFILE_HOST}'~g' /usr/local/openresty/nginx/conf/nginx.conf  && \
    sed -i 's~SEARCH_SERVICE_HOST~'${SEARCH_SERVICE_HOST}'~g' /usr/local/openresty/nginx/conf/nginx.conf && \
    sed -i 's~LOOKUP_SERVICE_HOST~'${LOOKUP_SERVICE_HOST}'~g' /usr/local/openresty/nginx/conf/nginx.conf && \
    sed -i 's~TENANT_HOST~'${TENANT_HOST}'~g' /usr/local/openresty/nginx/conf/nginx.conf && \
    nginx -g 'daemon off;'